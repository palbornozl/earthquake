# earthquake

### Description

Spring boot rest application to get earthquake using FDSN API.

* Required:
    * maven 3.6.2
    * jdk 1.8.0_211 or latest
* Compile:
    * mvn clean install inside of earthquake folder
* Run:
    * java -jar earthquake-0.0.1-SNAPSHOT.jar inside of earthquake/target folder
* Port: 8081
* Log: folder contain log_core.log

### Endpoints

 Description | Method | end point | Parameters | Example
 --- | --- | --- | --- | --- | 
 *Get earthquake between dates* | get | /gl/getInfoByTwoDates | starttime / endtime | http://localhost:8081/gl/getInfoByTwoDates?starttime=2019-01-01&endtime=2019-01-02
 *Get earthquake between range date* | get | /gl/getInfoByFourDates | starttime1 / endtime1 / starttime2 / endtime2 | http://localhost:8081/gl/getInfoByFourDates?starttime1=2019-01-01&endtime1=2019-01-02&starttime2=2019-02-01&endtime2=2019-02-02
 *Get earthquake between magnitudes* | get | /gl/getInfoByTwoMagnitudes | minmagnitude / maxmagnitude | http://localhost:8081/gl/getInfoByTwoMagnitudes?minmagnitude=6.6&maxmagnitude=8.5
 *Get list of earthquake by countries and between dates* | get | /gl/getInfoByCountriesAndDates | country1 / country2 / starttime1 / endtime1 / starttime2 / endtime2 | http://localhost:8081/gl/getInfoByCountriesAndDates?starttime1=2019-01-01&starttime2=2019-03-01&endtime2=2019-03-02&endtime1=2019-01-02&country1=chile&country2=puerto%20rico
 *GenerateAuthToken* | post | /gl/authenticate | none | http://localhost:8081/gl/authenticate

### Postman

Earthquake.postman_collection.json postman collection file to call eartquake RestAPI

### JWT

In postman collection has **GenerateAuthToken** request where will **generate Bearer token** which needed as 
an Authorization in others request.
