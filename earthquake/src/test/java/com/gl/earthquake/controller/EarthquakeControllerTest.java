package com.gl.earthquake.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.earthquake.model.Earthquake;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.gl.earthquake.utils.Constants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class EarthquakeControllerTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    @Test
    public void getInfoByTwoDatesTest() throws Exception {
        String uri = "/getInfoByTwoDates?" + DATE_START + "=2019-01-01&" + DATE_END + "=2019-01-02";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Earthquake[] earthquakes = mapFromJson(content, Earthquake[].class);
        assertTrue(earthquakes.length == 254, "earthquakes found: " + earthquakes.length);
    }

    @Test
    public void getInfoByFourDatesTest() throws Exception {
        String uri = "/getInfoByFourDates?" + DATE_START + "1=2019-01-01&" + DATE_END + "1=2019-01-02&" + DATE_START + "2=2019-03-01&" + DATE_END + "2=2019-03-02";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Earthquake[] earthquakes = mapFromJson(content, Earthquake[].class);
        assertTrue(earthquakes.length == 581, "earthquakes found: " + earthquakes.length);
    }

    @Test
    public void getInfoByTwoMagnitudesTest() throws Exception {
        String uri = "/getInfoByTwoMagnitudes?" + MAG_START + "=6.6&" + MAG_END + "=8.5";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Earthquake[] earthquakes = mapFromJson(content, Earthquake[].class);
        assertTrue(earthquakes.length == 3, "earthquakes found: " + earthquakes.length);
    }

    @Test
    public void getInfoByCountriesAndDatesTest() throws Exception {
        String uri = "/getInfoByCountriesAndDates?" +
                COUNTRY + "1=Chile&" + COUNTRY + "2=Puerto Rico&" +
                DATE_START + "1=2019-01-01&" + DATE_END + "1=2019-01-02&" +
                DATE_START + "2=2019-03-01&" + DATE_END + "2=2019-03-02";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Earthquake[] earthquakes = mapFromJson(content, Earthquake[].class);
        assertTrue(earthquakes.length == 13, "earthquakes found: " + earthquakes.length);
    }
}
