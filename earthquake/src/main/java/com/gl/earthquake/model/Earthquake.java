package com.gl.earthquake.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class Earthquake {

    private String place;
    private float magnitude;
    private Date date;

    @Override
    public String toString() {
        return "Earthquake{" +
                "place='" + place + '\'' +
                ", magnitude=" + magnitude +
                ", date=" + date +
                '}';
    }
}
