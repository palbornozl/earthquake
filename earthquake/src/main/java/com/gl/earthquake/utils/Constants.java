package com.gl.earthquake.utils;

public class Constants {

    public static final String FORMAT = "geojson";

    public static final String DATE_START = "starttime";

    public static final String DATE_END = "endtime";

    public static final String MAG_START = "minmagnitude";

    public static final String MAG_END = "maxmagnitude";

    public static final String COUNTRY = "country";
}
