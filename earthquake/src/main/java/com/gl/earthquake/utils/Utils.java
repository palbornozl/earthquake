package com.gl.earthquake.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.earthquake.model.Earthquake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Utils {

    public static void printInputParameters(MultiValueMap<String, String> allParams) {
        log.info("Input Parameters");
        allParams.forEach((k, v) -> log.info("Param [{}] - value [{}]", k, v));
    }

    public static HttpEntity getEntity() {
        log.info("Set rest HttpHeaders");
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity entity = new HttpEntity(headers);
        return entity;
    }

    public static void parsingResponseToList(List<Earthquake> earthquakeList, String response) throws JsonProcessingException {
        log.info("Parsing returned info in object");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response);
        AtomicInteger count = new AtomicInteger(1);
        root.get("features").forEach(f -> {
            Earthquake earthquake = new Earthquake();
            earthquake.setMagnitude(f.get("properties").get("mag").floatValue());
            earthquake.setDate(new Date(f.get("properties").get("time").asLong()));
            earthquake.setPlace(f.get("properties").get("place").asText().trim());
            earthquakeList.add(earthquake);
            log.debug("--> {}) country {}", count, f.get("properties").get("place").asText().trim());
            count.getAndIncrement();
        });

        log.info("Result found: {}", earthquakeList.size());

    }

    public static boolean isCountry(String country) {
        for (String countryCode : Locale.getISOCountries()) {
            Locale obj = new Locale("", countryCode);
            if (obj.getCountry().equalsIgnoreCase(country))
                return true;
            if (obj.getDisplayCountry().equalsIgnoreCase(country))
                return true;
        }
        return false;
    }
}
