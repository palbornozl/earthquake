package com.gl.earthquake.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gl.earthquake.exception.ResourceException;
import com.gl.earthquake.model.Earthquake;
import com.gl.earthquake.utils.Utils;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.LinkedList;
import java.util.List;

import static com.gl.earthquake.utils.Constants.*;

@RestController
@Slf4j
public class EarthquakeController {

    @Autowired
    RestTemplate restTemplate;

    @Value("${earthquake.api.query}")
    String earthquakeUrlDate;

    @ApiOperation(value = "Get list of Earthquake between dates ", response = Iterable.class, tags = "get earthquakes between dates")
    @GetMapping("/getInfoByTwoDates")
    public List<Earthquake> getInfoByTwoDates(@RequestParam MultiValueMap<String, String> allParams) throws ResourceException, JsonProcessingException {

        List<Earthquake> earthquakeList = new LinkedList<>();

        log.info("Get list of Earthquake between dates");

        validateTwoInputParameters(allParams, DATE_START, DATE_END);

        return getEarthquakes(allParams, earthquakeList, DATE_START, DATE_END);
    }

    @ApiOperation(value = "Get list of Earthquake 4 dates ", response = Iterable.class, tags = "get earthquakes 4 dates")
    @GetMapping("/getInfoByFourDates")
    public List<Earthquake> getInfoByFourDates(@RequestParam MultiValueMap<String, String> allParams) throws
            JsonProcessingException, ResourceException {

        List<Earthquake> earthquakeList = new LinkedList<>();

        log.info("Get list of Earthquake between dates");

        if (allParams.size() != 4 || !(allParams.containsKey(DATE_START + "1") &&
                allParams.containsKey(DATE_END + "1") &&
                allParams.containsKey(DATE_START + "2") &&
                allParams.containsKey(DATE_END + "2"))) {
            log.error("Something happened with input parameters, please check");
            throw new ResourceException("Input parameters are wrong, please check");
        }

        return getEarthquakes(allParams, earthquakeList, DATE_START, DATE_END);
    }

    @ApiOperation(value = "Get list of Earthquake between magnitudes ", response = Iterable.class, tags = "get earthquakes between magnitudes")
    @GetMapping("/getInfoByTwoMagnitudes")
    public List<Earthquake> getInfoByTwoMagnitudes(@RequestParam MultiValueMap<String, String> allParams) throws
            JsonProcessingException, ResourceException {

        List<Earthquake> earthquakeList = new LinkedList<>();

        log.info("Get list of Earthquake between magnitudes");

        validateTwoInputParameters(allParams, MAG_START, MAG_END);

        return getEarthquakes(allParams, earthquakeList, MAG_START, MAG_END);
    }

    @ApiOperation(value = "Get list of Earthquake by countries and between dates ", response = Iterable.class, tags = "get earthquakes by countries and between dates")
    @GetMapping("/getInfoByCountriesAndDates")
    public List<Earthquake> getInfoByCountriesAndDates(@RequestParam MultiValueMap<String, String> allParams) throws
            JsonProcessingException, ResourceException {

        List<Earthquake> earthquakeList = new LinkedList<>();

        log.info("Get list of Earthquake by countries and between dates");

        if (allParams.size() != 6 || !(allParams.containsKey(COUNTRY + "1") &&
                allParams.containsKey(COUNTRY + "2") &&
                allParams.containsKey(DATE_START + "1") &&
                allParams.containsKey(DATE_END + "1") &&
                allParams.containsKey(DATE_START + "2") &&
                allParams.containsKey(DATE_END + "2"))) {
            log.error("Something happened with input parameters, please check");
            throw new ResourceException("Input parameters are wrong, please check");
        }

        String country1 = allParams.getFirst(COUNTRY + "1");
        String country2 = allParams.getFirst(COUNTRY + "2");
        allParams.remove(COUNTRY + "1");
        allParams.remove(COUNTRY + "2");

        List<Earthquake> earthquakeListFinal = new LinkedList<>(getEarthquakes(allParams, earthquakeList, DATE_START, DATE_END));

        earthquakeList.forEach(e -> {
            String country;
            if (e.getPlace().split(",").length > 1) {
                country = e.getPlace().split(",")[1].trim();
                if (!(Utils.isCountry(country) && (country1.equalsIgnoreCase(country) ||
                        country2.equalsIgnoreCase(country)))) {
                    log.debug("No valid country {} ", country);
                    earthquakeListFinal.remove(e);
                }
            } else {
                log.debug("No valid country");
                earthquakeListFinal.remove(e);
            }
        });

        log.info("Final result found after filter country: {}", earthquakeListFinal.size());

        return earthquakeListFinal;
    }

    private List<Earthquake> getEarthquakes(@RequestParam MultiValueMap<String, String> allParams, List<Earthquake> earthquakeList, String init, String end) throws JsonProcessingException {

        Utils.printInputParameters(allParams);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(earthquakeUrlDate).queryParam("format", FORMAT);

        getInfoMultiParamPairs(earthquakeList, allParams, builder, init, end, allParams.size());

        log.info("Final result found: {}", earthquakeList.size());
        return earthquakeList;
    }

    private void getInfoMultiParamPairs(List<Earthquake> earthquakeList, MultiValueMap<String, String> allParams,
                                        UriComponentsBuilder builder, String init, String end, int numParam) throws
            JsonProcessingException {
        int count = 1;
        do {
            builder.queryParam(init, allParams.get(numParam == 2 ? init : init + count));
            builder.queryParam(end, allParams.get(numParam == 2 ? end : end + count));
            log.info("Getting info!");
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, Utils.getEntity(), String.class);
            log.info("Adding results");
            Utils.parsingResponseToList(earthquakeList, response.getBody());
            count++;
        } while (count < (numParam - 1));
        log.info("Result found final: {}", earthquakeList.size());

    }

    private void validateTwoInputParameters(MultiValueMap<String, String> allParams, String init, String end) throws ResourceException {
        if (allParams.size() != 2 || !(allParams.containsKey(init) && allParams.containsKey(end))) {
            log.error("Something happened with input parameters, please check");
            throw new ResourceException("Input parameters are wrong, please check");
        }
    }
}
